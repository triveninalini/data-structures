

class BinaryTree {
  constructor(props) {
    
    this.tree = [];
  }

  getLeftChild(parentIndex) {
    return (2 * parentIndex) + 1;
  }

  getRightChild(parentIndex) {
    return (2 * parentIndex) + 2;
  }

  getParent(childIndex) {
    return Math.floor((childIndex - 1) / 2);
  }
  
  //        1
  //    2     3
  // 4    5   6  8
  //9 7  0
  insert(val) {
    this.tree.push(val);
  }

  findCommonAncestors(val1, val2) {
    let childIndex1 = this.tree.indexOf(val1),
      childIndex2 = this.tree.indexOf(val2);
    let parentIndex1 = -1,
      parentIndex2 = -1;

    while (parentIndex1 !== 0 && parentIndex2 !== 0) {
      parentIndex1 = this.getParent(childIndex1);
      parentIndex2 = this.getParent(childIndex2);
      if (this.tree[parentIndex1] !== this.tree[parentIndex2]) {
        childIndex1 = parentIndex1;
        childIndex2 = parentIndex2;
      } else {
        break;
      }
    }
    if(parentIndex1 === 0) return this.tree[parentIndex1];
    if(parentIndex2 === 0) return this.tree[parentIndex2];
    return this.tree[parentIndex1];
  }
}
//[1,2,3,4,5,6,8,9,7,0] 
let bt = new BinaryTree();
bt.insert(1);
bt.insert(2);
bt.insert(3);
bt.insert(4);
bt.insert(5);
bt.insert(6);
bt.insert(8);
bt.insert(9);
bt.insert(7);
bt.insert(0);
console.log(bt.findCommonAncestors(8, 6))


// Given a stream of characters, find the first non-repeating character from stream. You need to tell the first non-repeating character in O(1) time at any moment.

function getNonRepeatingChar()