class MultipleStacks {
  constructor(n, size) {
    this.values = [];
    this.size = size;
    if(!n || !size) throw new Error('invalid data');
    for(let i = 0; i<n; i++) {
      this.values.push(new Array());
    }
  }

  push(data, stackIndex) {
    let stack = this.values[stackIndex];
    if(stackIndex >= this.size) throw new Error('stack does not exist');
    if(stack.length === this.size) throw new Error('Stack overflow');
    stack.push(data);
    this.values[stackIndex] = stack;
    return data;
  }

  pop(stackIndex) {
    if(stackIndex >= this.size) throw new Error('invalid stack')
    return this.values[stackIndex].pop();
  }
}

module.exports = MultipleStacks;