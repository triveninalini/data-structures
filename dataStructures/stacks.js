class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}

class Stack {
  constructor() {
    this.first = null;
    this.last = null;
    this.size = 0;
  }

  push(val) {
    let node = new Node(val);
    if (!this.last) this.last = node;
    else {
      node.next = this.first;
    }
    this.first = node;
    ++this.size;
    return this.size;
  }
  //3-1-2
  //2

  pop() {
    if (!this.size) return null;
    let node = this.first;
    if (this.size === 1) {
      this.last = null;
    }
    this.first = node.next;
    --this.size;
    return node;
  }

  getMax() {
    let node = this.head, max = -Infinity;
    while (node) {
      max = Math.max(max, node.val);
      node = node.next;
    }
    return Math.floor(Math.log10(Math.abs(max))) + 1
  }

  display() {
    let node = this.first;
    while (node) {
      console.log(node.val);
      node = node.next;
    }
  }
  //9, 90
  sort(first = this.first, last = this.last) {
    function pivotHelper(first) {
      let node = first, pivot = first, next = pivot;
      while (node.next !== last.next) {
        if (node.val < pivot.val) {
          next = next.next;
          let temp = next.val;
          next.val = node.val;
          node.val = temp;
        }
        node = node.next;
      }

      let temp = next.val;
      next.val = pivot.val;
      pivot.val = temp;
      return next;
    }
    if (first !== last) {
      last = pivotHelper(first, last);
      if (first !== last) {
        this.sort(first, last);
        this.sort(last, this.last);
      }
    }
  }
}
let stack = new Stack();
stack.push(10);
stack.push(1);
stack.push(100);
stack.push(9);
stack.push(90);
stack.sort();
module.exports = Stack;

//90, 10
//1, 90
