class HashTable {
  constructor(size=53) {
    this.keyValues = new Array(size);
  }

  _hash(key) {
    let total = 0; const prime = 31;
    for(let i = 0; i<Math.min(key.length, 100); i++) {
      let value = key[i].charCodeAt(0) -96;
      total = (total * prime +value) % prime;
    }
    return total;
  }

  set(key, value) {
    let result = [key, value]
    let index = this._hash(key);
    if(this.keyValues[index]) this.keyValues[index].push(result);
    else this.keyValues[index] = [result];
  }

  get(key) {
    let index = this._hash(key);
    if(!this.keyValues[index]) return undefined;
    if(this.keyValues[index].length > 1) {
      for(let i = 0; i< this.keyValues[index].length; i++) {
        if(this.keyValues[index][i][0] === key) {
          return this.keyValues[index][i][1];
        }
      }
    } else {

      return this.keyValues[index][0][1];
    }
  }
}

let ht = new HashTable();
ht.set('hello', 'hi')
ht.set('cyan', 1);
ht.set('salmon', 2)
console.log(ht.get('salmon'));
console.log(ht.get('hi'));
