class Node {
  constructor(val) {
    this.val = val;
    this.prev = null;
    this.next = null;
  }
}

class DoublyLinkedList {
  constructor() {
    this.head = null;
    this.head = null
    this.length = 0;
  }

  push(val) {
    let node = new Node(val);
    if (!this.head) {
      this.head = node;
    } else {
      this.tail.next = node;
      node.prev = this.tail;
    }
    this.tail = node;
    ++this.length;
    return node;
  }

  pop() {
    if(!this.tail) return undefined;
    let popNode = this.tail;
    if(this.length === 1) {
      this.head = null;
      this.tail = null;      
    }else {
      this.tail.prev.next = null;
      this.tail = this.tail.prev;
    } 
    popNode.prev = null;
    --this.length;
    return popNode;
  }

  shift() {
    if(!this.head) return undefined;
    let shiftNode = this.head;
    if(this.length === 1) {
      this.head = null;
      this.tail = null;
    } else {
      this.head = shiftNode.next;
      shiftNode.next = null;
      this.head.prev = null;
    }
    --this.length;
    return shiftNode;
  }

  unshift(val) {
    let newNode = new Node(val);
    if(!this.head) {
      this.tail = newNode;
    } else {
      newNode.next = this.head;
      this.head.prev = newNode;
    }
    this.head = newNode;
    ++this.length;
    return newNode;
  }

  get(index){
    if(index < 0 || index >= this.length) return null;
    if(index < this.length/2) {
      let count = 0, node = this.head
      while(count !== index) {
        node = node.next;
        count++;
      }
      return node;
    }
    let node = this.tail, count = this.length -index -1;
    while(count) {
      node = node.prev;
      --count;
    }
    return node;
  }

  set(index, val) {
    let node = this.get(index)
    if(node) node.val = val;
    return node;
  }

  insert(val, index) {
    if(index === 0) return !!this.unshift(val);
    if(index === this.length -1) return !!this.push(val)
    let currentNode = this.get(index);
    if(currentNode) {
      let newNode = new Node(val);
      newNode.next = currentNode;
      newNode.prev = currentNode.prev;
      currentNode.prev = newNode;
      newNode.prev.next = newNode;
      ++this.length;
      return true;
    }
    return false;
  }

  remove(index) {
    if(index === 0) return !!this.shift()
    if(index === this.length -1) return !!this.pop();
    let removeNode = this.get(index);
    if(removeNode) {
      removeNode.prev.next = removeNode.next;
      removeNode.next.prev = removeNode.prev;
      removeNode.prev = null;
      removeNode.next = null;
      --this.length;
      return true;
    }

    return false;
  }
}
//1,2,3,4,5,6,7,8,9
const d = new DoublyLinkedList();
d.push(10);
d.push(20);
d.push(30);
d.unshift(1)
d.unshift(2)
d.unshift(3)
//3,2,1,10,20,30
console.log(d.get(3));
console.log(d.remove(3));
console.log(d.get(3));
