const Stack = require('./stacks');
expect = require('chai').expect;

class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }

  equality(node) {
    return node.constructor.name === 'Node' && this.data === node.data && this.next === node.next;
  }
}


class SinglyLinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }
  push(data) {
    let node = new Node(data);
    if (this.length) {
      this.tail.next = node;
      this.tail = node;
    } else {
      this.head = node;
      this.tail = node;
    }
    this.length++;
    return this.length;
  }

  unshift(data) {
    let node = new Node(data);
    node.next = this.head;
    this.head = node;
    if (!this.tail) this.tail = this.head;
    ++this.length;
    return node;
  }

  shift() {
    if (!this.head) return null;
    let node = this.head;
    if (!this.head.next) {
      this.tail = null;
    }
    this.head = this.head.next;
    --this.length;
    return node;
  }
  pop() {
    if (!this.head) return this.length;
    if (this.tail === this.head) {
      this.tail = null;
      this.head = null;
    } else {
      let node = this.head;
      while (node.next !== this.tail) node = node.next;
      this.tail = node;
      this.tail.next = null;
    }
    return --this.length;
  }
  get(nodeIndex) {
    if (nodeIndex < 0 || !this.length || this.length <= nodeIndex) return undefined;
    let node = this.head
    for (let index = 0; index < nodeIndex; index++) {
      node = node.next;
    }
    return node;
  }
  set(data, nodeIndex) {
    let node = this.get(nodeIndex);
    if (node) {
      node.data = data;
      return true;
    }
    return false;
  }

  insert(data, index) {
    if (index === 0 || this.length === 0) return !!this.unshift(data);
    if (index === this.length) return !!this.push(data);
    let node = new Node(data);
    let currentNode = this.get(index);
    if (!currentNode) return false;
    node.next = currentNode;
    let prevNode = this.get(index - 1);
    prevNode.next = node;
    ++this.length;
    return true;
  }
  remove(index) {
    if (index < 0 || this.length <= index) return false;
    if (index === 0) return !!this.shift();
    if (index === this.length - 1) return !!this.pop();
    let nextNode = this.get(index + 1);
    let prevNode = this.get(index - 1);
    prevNode.next = nextNode;
    --this.length;
    return true;
  }
  //1,2,3,4,5
  //1,2,3,4,5
  reverse() {
    let currentNode = this.head, nextNode = null, prevNode = null;
    this.head = this.tail;
    this.tail = currentNode;
    for (let index = 0; index < this.length; index++) {
      nextNode = currentNode.next;
      currentNode.next = prevNode;
      prevNode = currentNode;
      currentNode = nextNode;
    }
    //swap head and tail
    //let tail be equal to head


  }

  getElementBasedOnIndex(index) {
    let node = this.head;
    while(index) {
      node = node.next;
      --index;
    }
    return node;
  }

  //1,2,3,4,5,6
  //4,5,6
  intersection(linkedList) {
    try {
      expect(linkedList.tail).to.be.eql(this.tail);
    } catch (ex) {
      return false;
    }
    let node1 = null, node2 = null;
    if (this.length > linkedList.length) {
      let count = this.length - linkedList.length;
      node1 = this.getElementBasedOnIndex(count);
      node2 = linkedList;
    } else {
      let count = linkedList.length - this.length;
      node1 = linkedList.getElementBasedOnIndex(count);
      node2 = this;
    }
    while (node1) {
      try {
        expect(node1).to.be.eql(node2.head);
        return true;
      } catch (ex) {

      } finally {
        if(!node2 || !node1) return false;
        node2 = node2.next;
        node1 = node1.next
      }
    }

    return false;
  }



  removeDuplicates() {
    let node = this.head;
    let visitedNode = {}, prev = null;
    while (node) {
      if (visitedNode[node.data]) {
        prev.next = node.next;
        if (!node.next) this.tail = prev;
        --this.length;
      } else {
        prev = node;
        visitedNode[node.data] = true;
      }
      node = node.next
    }

  }
  //6->3->1
  getNumber(l) {
    let node = l.head, result = 0, multiplier = 1;
    while (node) {
      result = result + (node.data * multiplier);
      node = node.next;
      multiplier = multiplier * 10
    }
    return result;
  }

  appendZero(bigList, smallList) {
    let count = bigList.length - smallList.length;
    for (let i = 0; i < count; i++) {
      smallList.push(0);
    }
  }
  //102, 2013
  sumList_better(l1, l2) {
    if (!l1 || !l1.length) return l2;
    if (!l2 || !l2.length) return l1;
    if (l1.length > l2.length) this.appendZero(l1, l2);
    if (l2.length > l1.length) this.appendZero(l2, l1);
    let result = new SinglyLinkedList(), carry = 0,
      node1 = l1.head, node2 = l2.head;
    while (node1 && node2) {
      let sum = carry + node1.data + node2.data;
      if (sum > 9) {
        carry = Math.floor(sum / 10);
        sum = sum % 10;
      }
      result.push(sum);
      node1 = node1.next;
      node2 = node2.next;
    }
    return result;
  }
//1,2,3,2,1,3,4
  isCircular() {
    let slow = this.head, fast = this.head;
    while(fast && fast.next) {
      slow = slow.next;
      fast = fast.next.next;
      if(slow == fast) {
        break;
      }
    }
    console.log(slow, fast.next);
  }
  sumList(l1, l2) {
    if (!l1 || !l1.length) return l2;
    if (!l2 || !l2.length) return l1;
    let num1 = this.getNumber(l1);
    let num2 = this.getNumber(l2);
    let result = num2 + num1;
    return this.createList(result)
  }
  //1024
  createList(num) {
    let ll = new SinglyLinkedList();
    while (num) {
      let digit = num % 10;
      ll.push(digit);
      num = Math.round(num / 10);
    }
    return ll;
  }

  palindrome() {

  }

  palindrome() {
    let mid = Math.floor(this.length / 2), stack = new Stack();
    let count = mid - 1, node = this.head;
    while (count >= 0) {
      stack.push(node);
      count--;
      node = node.next;
    }
    if (this.length % 2) node = node.next;
    while (count && node) {
      let temp = stack.pop();
      if (node.data !== (temp.val || {}).data) return false;
      node = node.next;
      count--;
    }
    return true;
  }
  //1221
  getLinkedListFromKthElement(k) {
    if (k >= this.length) return null;

    let result = new SinglyLinkedList(), count = 0, node = this.head;
    result.tail = result.tail;
    while (node) {
      if (count === k) {
        result.head = node; break;
      };
      node = node.next;
      count++;
    }
    return result;
  }
  //2,1
  palindrome_rec() {
    recursive = recursive.bind(this);
    function recursive(node, length) {
      if (!length) return { temp: node, result: true };
      if (length === 1) return { temp: node.next, result: true };
      let { temp, result } = recursive(node.next, length - 2);
      if (!result) return { temp: null, result }
      result = temp.data === node.data;
      return { temp: temp.next, result }
    }

    let data = recursive(this.head, this.length);
    return data.result;
  }
}
//let l2 = ll.intersection(ll1, ll);
module.exports = SinglyLinkedList;
//2222+0012 = 2234