class Node {
  constructor(priority, value) {
    this.priority = priority;
    this.value = value;
  }
}

class PriorityQueue {
  constructor() {
    this.values = []
  }
  getLeftChild(parentIndex) {
    return 2 * parentIndex + 1;
  }

  getRightChild(parentIndex) {
    return 2 * parentIndex + 2;
  }
  getParent(childIndex) {
    return Math.floor((childIndex - 1) / 2);
  }

  swap(parentIndex, childIndex = this.values.length - 1) {
    let temp = this.values[parentIndex];
    this.values[parentIndex] = this.values[childIndex];
    this.values[childIndex] = temp;
  }

  bubbleUp() {
    let childIndex = this.values.length - 1;
    let parentIndex = this.getParent(childIndex);
    let parentNode = this.values[parentIndex], childNode = this.values[childIndex];
    while (parentNode.priority > childNode.priority) {
      this.swap(parentIndex, childIndex);
      childIndex = parentIndex;
      if (!childIndex) break;
      parentIndex = this.getParent(childIndex);
      parentNode = this.values[parentIndex];
      childNode = this.values[childIndex];
    }
  }

  roundup() {
    let parentIndex = 0, childIndex = this.getLeftChild(parentIndex);
    
    let parentNode = this.values[parentIndex],
      leftchildNode = this.values[childIndex], rightChildNode = this.values[childIndex + 1];
    while (childIndex < this.values.length) {
      console.log(parentIndex, childIndex);
      if(!rightChildNode && parentNode.priority < leftchildNode.priority) break;
      if (parentNode.priority < leftchildNode.priority && 
        parentNode.priority < rightChildNode.priority) break;
      if (rightChildNode && leftchildNode.priority > rightChildNode.priority)++childIndex;
      if (parentNode.priority > this.values[childIndex].priority) {
        this.swap(parentIndex, childIndex);
        parentIndex = childIndex;
        childIndex = this.getLeftChild(parentIndex);
        parentNode = this.values[parentIndex],
          leftchildNode = this.values[childIndex], rightChildNode = this.values[childIndex + 1];
      }
    }
  }

  dequeue() {
    if (!this.values.length) return undefined;
    this.swap(0);
    let result = this.values.pop()
    if (this.values.length === 1) return result;
    this.roundup();
    return result;
  }

  enqueue(priority, value) {
    let node = new Node(priority, value);
    this.values.push(node);
    if (this.values.length === 1) return this.values;
    this.bubbleUp()
  }
}

let p = new PriorityQueue();
p.enqueue(10, 10);
p.enqueue(20, 10);
p.enqueue(5, 10);
p.enqueue(2, 10);
p.enqueue(1, 10);
p.enqueue(8, 10);
p.dequeue();
console.log(p);
p.dequeue();
console.log(p);