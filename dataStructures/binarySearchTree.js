const Queue = require('../dataStructures/queue');
const SinglyLinkedList = require('../dataStructures/linkedList');

class Node {
  constructor(val) {
    this.val = val;
    this.right = null;
    this.left = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  async insert(val) {
    let node = new Node(val);
    const assignNode = (parent = this.root) => {

      if (!this.root) {
        this.root = node;
        return;
      };
      if (parent.val === val) {
        node = null;
        return;
      }

      if (parent.val > node.val) {
        parent.left ? assignNode(parent.left) : parent.left = node;
      } else {
        parent.right ? assignNode(parent.right) : parent.right = node;
      }
    }
    await assignNode(this.root);
    return node;
  }

  find(val) {
    let parent = this.root;
    while (parent) {
      if (parent.val === val) return parent;
      parent = parent.val < val ? parent.right : parent.left;
    }
    return null;
  }
  //breadth first search
  bfa() {
    if (!this.root) return [];
    let queue = new Queue(), result = [];
    queue.enqueue(this.root);
    while (queue.size) {
      let currentQueue = queue.dequeue();
      let currentTree = (currentQueue || {}).val
      if (currentTree) {
        result.push(currentTree.val);
        if (currentTree.left) queue.enqueue(currentTree.left);
        if (currentQueue.val.right) queue.enqueue(currentTree.right);
      }
    }
    return result;
  }

  //depth first search
  dfsPreOrder() {
    if (!this.root) return [];
    let currentNode = this.root, result = [];
    function helper(node) {
      result.push(node.val);
      if (node.left) helper(node.left);
      if (node.right) helper(node.right);
    }
    helper(currentNode);
    return result;
  }

  isNodeBalanced(node) {
    try {
      if (!node.left) {
        if (!node.right) return true;
        if (node.right.left || node.right.right) return false;
      }
      if (!node.right) {
        if (!node.left) return true;
        if (node.left.left || node.left.right) return false;
      }
      return true;
    } catch (ex) {
      return false;
    }

  }

  calculateHeight(node, valid = true, leftHeight = -Infinity) {
    let height = 1, dataValid = Math.abs(height - leftHeight);
    if (dataValid !== Infinity && dataValid > 1) return { height, valid: false };
    if (!node && !valid) return { height: 0, valid };
    valid = this.isNodeBalanced(node)
    if (node.left) height += this.calculateHeight(node.left, valid, leftHeight).height;
    if (node.right) height += this.calculateHeight(node.right, valid, leftHeight).height;
    return { height, valid };
  }

  isTreeBalanced() {
    if (!this.isNodeBalanced(this.root)) return false;
    let leftData = this.calculateHeight(this.root.left);
    if (!leftData.valid) return false;
    let rightData = this.calculateHeight(this.root.right, true, leftData.height);
    return rightData.valid;
    //find left height

    //find right height
  }

  findTheDiameter(node = this.root, max = -Infinity) {
    let left = {}, right = {},  count = 1, path = new SinglyLinkedList();
    if (node.left) {
      left = this.findTheDiameter(node.left, max);
      path.unshift()
    }
    if (node.right) {
      right = this.findTheDiameter(node.right, max);
    }
    count += (left.count || 0) + (right.count || 0);
    max = Math.max(max, count);
    return { count, path: [...(left.path || []), ...(right.path || [])]}
  }

  dfsPostOrder() {
    if (!this.root) return [];
    let currentNode = this.root, result = [];
    function helper(node) {
      if (node.left) helper(node.left);
      if (node.right) helper(node.right);
      result.push(node.val);
    }
    helper(currentNode);
    return result;
  }

  successor(val) {
    if (!this.root) return null;
    let node = this.root;
    while (node) {
      if (node.val === val) return node;
      if (node.val > val) node = node.left
      else if (node.val < val) node = node.right;
    }
    return null;
  }

  dfsInOrder() {
    if (!this.root) return [];
    let currentNode = this.root, result = [];
    function helper(node) {
      if (node.left) helper(node.left);
      result.push(node.val);
      if (node.right) helper(node.right);
    }
    helper(currentNode);
    return result;
  }
  //
  possibleArr(bst) {
    if (!bst.root) return [];
    let arr = [];


    function leftTranverse(node = bst.root, result = []) {
      if (node === bst.root) result.push(node.val);
      if (node) {
        if (node.left) result.push(node.left.val);
        if (node.right) result.push(node.right.val);
        leftTranverse(node.left);
        leftTranverse(node.right);
      }
      return result;
    }
    function rightTransverse(node = bst.root, result = []) {
      if (node === bst.root) result.push(node.val);
      if (node) {
        if (node.right) result.push(node.right.val);
        if (node.left) result.push(node.left.val);
        rightTransverse(node.right);
        rightTransverse(node.left);
      }
      return result
    }
    arr.push(leftTranverse());
    arr.push(rightTransverse());
    return arr;
  }
}

let bst = new BinarySearchTree();
bst.insert(2);
bst.insert(1);
bst.insert(3);
console.log(bst.findTheDiameter(bst.root));
// //10
// 8 20
//5 9 15 25, -----[5, 8 ,10]
//bst.dfs()

module.exports = BinarySearchTree;

//10
//15 25
//5  8     35
