class MaxBinaryHeaps {
  constructor() {
    this.values = [];
  }

  getLeftChild(parentIndex) {
    return 2 * parentIndex + 1;
  }

  getRightChild(parentIndex) {
    return 2 * parentIndex + 2;
  }
  getParent(childIndex) {
    return Math.floor((childIndex - 1) / 2);
  }


  swap(parentIndex, childIndex = this.values.length - 1) {
    let temp = this.values[parentIndex];
    this.values[parentIndex] = this.values[childIndex];
    this.values[childIndex] = temp;
  }

  bubbleUp() {
    let index = this.values.length - 1, parentIndex = this.getParent(index),
      val = this.values[index];
    while (this.values[parentIndex] < val) {
      this.swap(parentIndex, index);
      index = parentIndex;
      parentIndex = this.getParent(parentIndex);
    }
  }

  roundUp() {
    let parentIndex = 0,
      childIndex = this.getLeftChild(parentIndex);
    while (childIndex < this.values.length) {
      if (this.values[parentIndex] > this.values[childIndex] &&
        (this.values[parentIndex] > this.values[childIndex + 1] || -Infinity)) break;
      if (this.values[childIndex + 1] > this.values[childIndex])++childIndex;
      this.swap(parentIndex, childIndex);
      parentIndex = childIndex;
      childIndex = this.getLeftChild(parentIndex);
    }

  }

  extractMax() {
    if (!this.values.length) return undefined;
    if (this.values.length === 1) return this.values.pop();
    this.swap(0);
    if (this.values.length === 2) return this.values.pop();
    let result = this.values.pop();
    this.roundUp();
    return result;
  }



  async insert(value) {
    this.values.push(value);
    if (this.values.length <= 1) return this.values;
    await this.bubbleUp();
    return this.values;
  }

}
// let heap = new MaxBinaryHeaps();
// heap.insert(100);
// heap.insert(200);
// heap.insert(50);
// heap.insert(75);
// heap.insert(120);
// let result = heap.insert(250);
// heap.extractMax();
// heap.extractMax();
// console.log(heap)
// heap.extractMax();
// console.log(heap)
module.exports = MaxBinaryHeaps;