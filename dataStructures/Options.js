class Node {
  constructor(option, level) {
    this.option = option;
    this.level = level;
  }
}

class Option {
  constructor() {
    this.values = []
  }

  getParent(n) {
    return Math.floor((n-1)/2);
  }


  swap(parentIndex, childIndex = this.values.length - 1) {
    let temp = this.values[parentIndex];
    this.values[parentIndex] = this.values[childIndex];
    this.values[childIndex] = temp;
  }

  bubbleUp() {
    let childIndex = this.values.length -1;
    let parentIndex = this.getParent(childIndex);
    while(this.values[parentIndex].level > this.values[childIndex].level) {
      this.swap(parentIndex, childIndex);
      childIndex = parentIndex;
      parentIndex = this.getParent(childIndex);
    }
  }

  insert(option, level = 1.0) {
    let node = new Node(option, level);
    this.values.push(node);
    if(this.values.length === 1) return this.values;
    this.bubbleUp()
  }
}

let option  = new Option();
option.insert(10);
option.insert(10, 1.00001);
option.insert(10, 1.2);
option.insert(10, 1.1);
console.log(option);