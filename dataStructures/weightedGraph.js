const Queue = require('./priorityQueueForDikshatra');
class WeightedGraph {
  constructor() {
    this.adjacencyList = {}
  }

  addVertex(vertex) {
    if (!this.adjacencyList[vertex]) this.adjacencyList[vertex] = [];
  }

  addEdje(vertex1, vertex2, weight) {
    let node1 = this.adjacencyList[vertex1] || [];
    let node2 = this.adjacencyList[vertex2] || [];
    node1.push({ node: vertex2, weight });
    node2.push({ node: vertex1, weight });
    this.adjacencyList[vertex1] = node1;
    this.adjacencyList[vertex2] = node2;
  }

  dijkstra(start, end) {
    let distance = {}, previous = {}, queue = new Queue(),
      visited = {};
    for (let key in this.adjacencyList) {
      if (this.adjacencyList.hasOwnProperty(key))
        if (key !== start) {
          distance[key] = Infinity;
          queue.enqueue(key, Infinity)

        } else {
          queue.enqueue(key, 0);
          distance[key] = 0;
        };
      previous[key] = null
    }
    console.log(queue);
    while ((queue.values || []).length) {
      let small = queue.dequeue();
      if (small.node === end) break;
      if (small && distance[small.node] !== Infinity) {
        let neighbours = this.adjacencyList[small.node] || [];
        for (let i = 0; i < neighbours.length; i++) {
          let neighbour = neighbours[i];
          let sum = neighbour.weight + distance[small.node];
          if(sum < distance[neighbour.node]) {
            previous[neighbour.node] = small.node;
            distance[neighbour.node] = sum;
            queue.enqueue(neighbour.node, sum);
          }
          
        }
      }

    }
    console.log(distance);
    return distance;
  }
}

//a
//b c
//d
let w = new WeightedGraph();
w.addEdje('A', 'B', 4);
w.addEdje('A', 'C', 2);
w.addEdje('C', 'D', 2);
w.addEdje('C', 'F', 4);
w.addEdje('D', 'E', 3);
w.addEdje('D', 'F', 1);
w.addEdje('E', 'F', 1);
w.addEdje('B', 'E', 3);
console.log(w.dijkstra('A', 'E'))
module.exports = WeightedGraph;
