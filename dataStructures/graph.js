const SinglyLinkedList = require('./linkedList');

class Graph {
  constructor() {
    this.adjacencyList = {}
  }

  addVertex(vertex) {
    if (!this.adjacencyList[vertex]) this.adjacencyList[vertex] = [];
  }

  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  addEdge(vertex1, vertex2) {
    let arr1 = this.adjacencyList[vertex1] || [],
      arr2 = this.adjacencyList[vertex2] || [];
    if (arr1.indexOf(vertex2) < 0) arr1.push(vertex2);
    if (arr2.indexOf(vertex1) < 0) arr2.push(vertex1);
    this.adjacencyList[vertex1] = arr1;
    this.adjacencyList[vertex2] = arr2;
  }

  removeEdge(vertex1, vertex2) {
    let arr1 = this.adjacencyList[vertex1] || [],
      arr2 = this.adjacencyList[vertex2] || [];
    if (arr1.indexOf(vertex2) > -1) arr1.splice(arr1.indexOf(vertex2), 1)
    if (arr2.indexOf(vertex1) > -1) arr2.splice(arr2.indexOf(vertex1), 1)
  }

  removeVertex(vertex) {
    delete this.adjacencyList[vertex];
    for (let key in this.adjacencyList) {
      let edge = this.adjacencyList[key] || [];
      edge = edge.filter(v => v !== vertex);
      this.adjacencyList[key] = edge;
    }
  }

  //depth first approach recursively
  bfs() {
    let result = [], visitedNode = {};
    for (let key in this.adjacencyList) {
      if (this.adjacencyList.hasOwnProperty(key) && !visitedNode[key]) {
        result.push(key);
        visitedNode[key] = true;
        let list = this.adjacencyList[key];
        for (let index = 0; index < list.length; index++) {
          if (!visitedNode[list[index]]) {
            result.push(list[index]);
            visitedNode[[list[index]]] = true
          }
        }
      }
    }

    return result;
  }

  getGraphList() {
    //create a singly linked list and visited node obj
    //loop through adjacent list
    //check key is in visited node obj
    //if it is not add that object to linked list
    //add that key to visited node

    let ll = new SinglyLinkedList();
    for (let key in this.adjacencyList) {

      ll.insert(key);

    }
    return ll;
  }

}
let g = new Graph();
g.addEdge('d','a');
g.addEdge('b', 'f');
g.addEdge('d', 'b');
g.addEdge('c', 'd');
g.addEdge('a', 'f');
g.addVertex('e');
console.log(g.bfs())
module.exports = Graph;
//c->d->a->b