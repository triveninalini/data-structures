const expect = require('chai').expect,
  MaxBinaryHeaps = require('../dataStructures/maxBinaryHeaps');

// begin a test suite of one or more tests
describe('Max binary search', function () {
  let heap = new MaxBinaryHeaps();
  heapResult = [ 250, 200, 90, 120, 100, 50, 45, 75 ];
  it('testing extract max--->when there is no element', async () => {
    let values = await heap.extractMax();
    expect(values).to.be.eql(undefined);
    expect(heap.values.length).to.be.eql(0);
    expect(heap.values).to.eql([]);
  });
  it('inserting first element', async () => {
    let values = await heap.insert(100);
    expect(values).to.be.instanceOf(Array);
    expect(values.length).to.be.eql(1);
    expect(values[0]).to.be.eql(100);
    expect(heap.values).to.be.eql([100])
  });

  it('testing extract max--->when there is no element', async () => {
    let values = await heap.extractMax();
    expect(values).to.be.eql(100);
    expect(heap.values.length).to.be.eql(0);
    expect(heap.values).to.be.eql([])
    await heap.insert(100);
  });

  it('inserting elemnent greated than root', async () => {
    let values = await heap.insert(200);
    expect(values).to.be.instanceOf(Array);
    expect(values.length).to.be.eql(2);
    expect(values[0]).to.be.eql(200);
    expect(heap.values).to.be.eql([200, 100])
  });

  it('testing extract max--->when there is two element', async () => {
    let values = await heap.extractMax();
    console.log(heap, values)
    expect(values).to.be.eql(200);
    expect(heap.values.length).to.be.eql(1);
    expect(heap.values).to.be.eql([100]);
    await heap.insert(200);
  });

  //[250, 200, 120, 90, 75, 100, 50, 45]

  it('inserting element one or more elements', async () => {
    await heap.insert(50);
    await heap.insert(75);
    await heap.insert(120);
    await heap.insert(90);
    await heap.insert(45);
    let result = await heap.insert(250);
    expect(result).to.eql(heapResult);
  })
  it('testing extract max--->when there is two or more element', async () => {
    let values = await heap.extractMax();
    expect(values).to.be.eql(250);
    expect(heap.values.length).to.be.eql(7);
    expect(heap.values).to.be.eql([200, 120, 90, 75, 100, 50, 45]);
    await heap.insert(200);
  });
  
});
//250
//90 120
//100 50 45 75


//200
//120 90
//75 100 50 45

//250
//200 90
//120 100 50 45
//75