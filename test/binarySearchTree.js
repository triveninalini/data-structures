const moch = require('mocha'),
  expect = require('chai').expect,
  BinarySearchTree = require('../dataStructures/binarySearchTree');

// begin a test suite of one or more tests
describe('Binary search', function () {
  let bst = new BinarySearchTree(),
    bfs = [100, 90, 110, 85, 95, 105, 115],
    dfsPreOrder = [100, 90, 85, 95, 110, 105, 115],
    dfsInOrder = [85, 90, 95, 100, 105, 110, 115],
    dfsPostOrder = [85, 95, 90, 105, 115, 110, 100];
//100,90,110,95,85,115,105
    //100
//90 110
//85 95 105 115------>[100, 90, 110, 85, 95, 115]

  it('breadth first approach---> this.root is null', async () => {
    let result = await bst.bfa();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(0);
  });

  it('depth first approach, pre order---> this.root is null', async () => {
    let result = await bst.dfsPreOrder();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(0);
  });

  it('depth first approach, post order---> this.root is null', async () => {
    let result = await bst.dfsPostOrder();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(0);
  });

  it('inserting first element', async () => {
    let node = await bst.insert(100);
    expect(node).to.eql(bst.root);
  });

  it('breadth first approach---> adding first element', async () => {
    let result = await bst.bfa();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(1);
    expect(result[0]).to.be.eql(100)
  });

  it('depth first approach, pre order---> adding first element', async () => {
    let result = await bst.dfsPreOrder();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(1);
    expect(result[0]).to.be.eql(100);
  });

  it('depth first approach, post order---> adding first element', async () => {
    let result = await bst.dfsPostOrder();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(1);
    expect(result[0]).to.be.eql(100)
  });

  it('inserting smaller element', async () => {
    let node = await bst.insert(90);
    expect(node).to.eql(bst.root.left);
  });
  it('inserting larger element', async () => {
    let node = await bst.insert(110);
    expect(node).to.eql(bst.root.right);
  });
  it('inserting  element to second left parent--->larger', async () => {
    let node = await bst.insert(95);
    expect(node).to.eql(bst.root.left.right);
  });
  it('inserting  element to second left parent---->smaller', async () => {
    let node = await bst.insert(85);
    expect(node).to.eql(bst.root.left.left);
  });
  it('inserting  element to second right parent---->larger', async () => {
    let node = await bst.insert(115);
    expect(node).to.eql(bst.root.right.right);
  });
  it('inserting  element to second right parent---->smaller', async () => {
    let node = await bst.insert(105);
    expect(node).to.eql(bst.root.right.left);
  });

  it('inserting first element', async () => {
    let node = await bst.insert(100);
    expect(node).to.be.null;
  });
  it('inserting first element', async () => {
    let node = await bst.insert(105);
    expect(node).to.be.null;
  });
  it('inserting first element', async () => {
    let node = await bst.insert(110);
    expect(node).to.be.null;
  });
  it('find a node', async () => {
    let node = await bst.find(110);
    expect(node.val).to.eql(110);
  });
  it('find a node', async () => {
    let node = await bst.find(85);
    expect(node.val).to.eql(85);
  });
  it('find a node', async () => {
    let node = await bst.find(70);
    expect(node).to.eql(null);
  });

  it('breadth first approach---> getting one or more elements', async () => {
    let result = await bst.bfa();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(7);
    expect(result).to.be.eql(bfs);
  });

  it('depth first approach, pre order---> getting one or more elements', async () => {
    let result = await bst.dfsPreOrder();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(7);
    expect(result).to.eql(dfsPreOrder);
    console.log(result)
  });

  it('depth first approach, post order---> getting one or more elements', async () => {
    let result = await bst.dfsPostOrder();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(7);
    expect(result).to.eql(dfsPostOrder);
  });

  it('depth first approach, in order---> getting one or more elements', async () => {
    let result = await bst.dfsInOrder();
    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.be.eql(7);
    expect(result).to.eql(dfsInOrder);
  });

  it('node is balanced---> empty tree', () => {
    let balancedBST = new BinarySearchTree();
    balancedBST.insert(10);
    let result = balancedBST.isNodeBalanced(balancedBST);
    expect(result).to.be.true;
  });

  it('node is balanced---> empty node', () => {
    let balancedBST = new BinarySearchTree();
    let result = balancedBST.isNodeBalanced(balancedBST);
    expect(result).to.be.false;
  });

  it('node is balanced----> node with no child', () => {
    let balancedBST = new BinarySearchTree();
    balancedBST.insert(10);
    let result = balancedBST.isNodeBalanced(balancedBST.root);
    expect(result).to.be.true;
  });

  it('node is balanced---> node with only left child', () => {
    let balancedBST = new BinarySearchTree();
    balancedBST.insert(10);
    balancedBST.insert(5);
    let result = balancedBST.isNodeBalanced(balancedBST.root);
    expect(result).to.be.true;
  });

  it('node is balanced---> node with only right child', () => {
    let balancedBST = new BinarySearchTree();
    balancedBST.insert(15);
    let result = balancedBST.isNodeBalanced(balancedBST.root);
    expect(result).to.be.true;
  });

  it('node is balanced---> node with childs', () => {
    let balancedBST = new BinarySearchTree();
    balancedBST.insert(10);
    balancedBST.insert(15);
    balancedBST.insert(5);
    let result = balancedBST.isNodeBalanced(balancedBST.root);
    expect(result).to.be.true;
  });

  it('is tree balanced --->with left and right node', () => {
    let balancedBST = new BinarySearchTree();
    balancedBST.insert(10);
    balancedBST.insert(15);
    balancedBST.insert(5);
    let result = balancedBST.isTreeBalanced();
    expect(result).to.be.true;
  });

  

});

//bst.insert(105);
