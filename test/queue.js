const moch = require('mocha'),
  expect = require('chai').expect,
  Queue = require('../dataStructures/queue');

// begin a test suite of one or more tests
describe('Queue', function () {
  let queue = new Queue();

  function checkLength(result, length) {
    expect(result).to.equal(queue.size);
    expect(queue.size).to.eql(length);
  }

  it('dequeue from empty queue', () => {
    let result = queue.dequeue();
    expect(result).to.be.null;
    expect(queue.size).to.eql(0);
    expect(queue.first).to.be.null;
    expect(queue.last).to.be.null;
  });

  it('adding first element', () => {
    let result = queue.enqueue(10);
    checkLength(result, 1);
    expect(queue.first).not.null;
    expect(queue.last).not.null;
    expect(queue.first.next).to.null;
    expect(queue.last.next).to.null;
  });

  it('removing first element', () => {
    let first = queue.first;
    let node = queue.dequeue();
    expect(queue.size).to.eql(0);
    expect(queue.first).to.be.null;
    expect(queue.last).to.be.null;
    expect(node).to.eql(first);
    queue.enqueue(10);
  })

  it('adding second element', () => {
    const last = { ...queue.last }; let temp = queue.first;
    let result = queue.enqueue(20);
    checkLength(result, 2);
    expect(queue.first).not.null;
    expect(queue.last).not.null;
    while (temp.next === last) {
      temp = temp.next;
    }
    expect(temp).to.not.eql(last);
    expect(queue.last).to.not.eql(last);
    expect(queue.last.next).to.null
  });


  it('removing one or more elements', () => {
    let first = {...queue.first};
    let node = queue.dequeue();
    expect(queue.size).to.eql(1);
    expect(queue.first).to.eql(first.next);
    expect(node).to.eql(first);
  });
});