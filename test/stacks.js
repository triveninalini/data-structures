const moch = require('mocha'),
  expect = require('chai').expect,
  Stack = require('../dataStructures/stacks');

// begin a test suite of one or more tests
describe('Stack', function () {
  let stack = new Stack();
  // add a test hook

  function checkLength(result, length) {
    expect(result).to.equal(stack.size);
    expect(stack.size).to.eql(length);
  }

  it('pop when there is no element', function () {
    let result = stack.pop();
    expect(result).to.be.null;
    expect(stack.size).to.eql(0);
    expect(stack.first).to.be.null;
    expect(stack.last).to.be.null;
  });

  // test a functionality
  it('adding first element', function () {
    let result = stack.push(10);
    checkLength(result, 1)
    expect(stack.first).not.null;
    expect(stack.last).not.null;
    expect(stack.first.next).to.null;
    expect(stack.first.next).to.null;
  });

  it('pop when there is one element', function () {
    let first = stack.first;
    let node = stack.pop();
    expect(stack.size).to.eql(0);
    expect(stack.first).to.be.null;
    expect(stack.last).to.be.null;
    expect(node).to.eql(first);
    stack.push(10);
  });

  it('adding second element', function () {
    const first = stack.first,
      result = stack.push(20);
    checkLength(result, 2);
    expect(stack.first.next).to.eql(first);
    expect(stack.first).not.to.eql(first);
  });

  it('pop when there is one or more elements', function () {
    const first = stack.first, node = stack.pop();
    expect(node).to.eql(first);
    expect(stack.first).to.eql(first.next);
    expect(stack.size).to.eql(1);
  });

  // it('sort', function () {
  //   stack.push(1);
  //   stack.push(100);
  //   stack.push(9);
  //   stack.push(90);
  //   console.log(stack.display());
  //   let result = stack.sort();
  //   console.log(stack.display());
  //   let node = stack.first;
  //   while(node.val !== 90) {
  //     expect(node.val < 90).to.be.true;
  //     node = node.next;
  //   }
  //   node = node.next;
  //   while(node) {
  //     expect(node.val > 90).to.be.true;
  //     node = node.next;
  //   }
  //   expect(result.val).to.be.eql(90);
  // });


  // ...some more tests

})

//1, 2, 3, 4, 5