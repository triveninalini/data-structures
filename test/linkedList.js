const LinkedList = require('../dataStructures/linkedList'),
  expect = require('chai').expect;

describe('Linked list', function () {
  //1,3,10,100,20,90
  let ll = new LinkedList(),
    l1 = new LinkedList(), l2 = new LinkedList();
  l1.push(1);
  l1.push(3);
  l1.push(2);
  l2.push(4);
  l2.push(3);
  l2.push(5);

  it('push removing duplicates---> only two data', async () => {
    ll.push(10);
    ll.removeDuplicates();
    expect(ll.length).to.be.eql(1);
    expect(ll.tail).to.be.eql(ll.head);
  });

  it('push removing duplicates---> only two data', async () => {
    ll.push(10);
    ll.removeDuplicates();
    expect(ll.length).to.be.eql(1);
    expect(ll.tail).to.be.eql(ll.head);
  })

  it('push removing duplicates---> big list data', async () => {
    ll.push(20);
    ll.push(1);
    ll.push(100);
    ll.push(3);
    ll.push(90);
    ll.push(3)
    ll.push(20);
    ll.removeDuplicates();
    expect(ll.length).to.be.eql(6);
    expect(ll.tail.data).to.be.eql(90);
  });

  it('breadth first approach, recursively------> no element found', async () => {
    ll.push(20);
    ll.push(1);
    ll.push(100);
    ll.push(3);
    ll.push(90);
    ll.push(3)
    ll.push(20);
    ll.removeDuplicates();
    expect(ll.length).to.be.eql(6);
    expect(ll.tail.data).to.be.eql(90);
  });

  it('removing kth element---> index is eql or greater than length', () => {
    let result = ll.getLinkedListFromKthElement(6);
    expect(result).to.be.null;
  });
  // 10,20,1,100,3,90

  it('sum list', () => {
    let result = ll.sumList(l1);
    expect(result).to.be.eql(l1);
  });

  it('sum list', () => {
    let result = ll.sumList(null, l1);
    expect(result).to.be.eql(l1);
  });

  it('sum list', () => {
    let result = ll.sumList(l1, l2);
  });
  //12321, 1221
  it('palindrome---> even digit data', () => {
    let linkedList = new LinkedList();
    linkedList.push(1)
    linkedList.push(2)
    linkedList.push(2)
    linkedList.push(1)
    let result = linkedList.palindrome();
    expect(result).to.be.eql(true);
  })

  it('palindrome---> odd digit data', () => {
    let linkedList = new LinkedList();
    linkedList.push(1)
    linkedList.push(2)
    linkedList.push(3)
    linkedList.push(2)
    linkedList.push(1)
    let result = linkedList.palindrome();
    expect(result).to.be.eql(true);
  })


  it('palindrome---> odd digit data', () => {
    let linkedList = new LinkedList();
    linkedList.push(1)
    linkedList.push(1)
    linkedList.push(3)
    linkedList.push(2)
    linkedList.push(1)
    let result = linkedList.palindrome();
    try {
      expect(result).to.be.eql(false);
    } catch (e) {
      console.error(e);
    }
  });

  it('palindrome---> odd digit data', () => {
    let linkedList = new LinkedList();
    linkedList.push(1)
    linkedList.push(2)
    linkedList.push(3)
    linkedList.push(2)
    linkedList.push(1)
    let result = linkedList.palindrome_rec();
    expect(result).to.be.eql(true);
  });

  it('palindrome---> even digit data', () => {
    let linkedList = new LinkedList();
    linkedList.push(1)
    linkedList.push(2)
    linkedList.push(2)
    linkedList.push(1)
    let result = linkedList.palindrome_rec();
    expect(result).to.be.eql(true);
  })

  it('palindrome---> odd digit data', () => {
    let linkedList = new LinkedList();
    linkedList.push(1)
    linkedList.push(1)
    linkedList.push(1)
    linkedList.push(1)
    linkedList.push(2)
    let result = linkedList.palindrome_rec();
    expect(result).to.be.eql(false);
  });

  it('intersection 1', () => {
    let linkedList1 = new LinkedList(), linkedList2 = new LinkedList();
    linkedList1.push(1); linkedList2.push(2); linkedList2.push(1);
    linkedList1.push(2)
    linkedList1.push(3)
    linkedList1.push(2)
    linkedList1.push(1)
    let result = linkedList1.intersection(linkedList2);
    expect(result).to.be.eql(true);
  });

  it('intersection 2', () => {
    let linkedList1 = new LinkedList(), linkedList2 = new LinkedList();
    linkedList1.push(1); linkedList2.push(1); linkedList2.push(1);
    linkedList1.push(2)
    linkedList1.push(3)
    linkedList1.push(2)
    linkedList1.push(1)
    let result = linkedList1.intersection(linkedList2);
    expect(result).to.be.eql(false);
  });

  it('intersection 3', () => {
    let linkedList1 = new LinkedList(), linkedList2 = new LinkedList();
    linkedList1.push(1); linkedList2.push(2); linkedList2.push(1);
    linkedList1.push(2);linkedList2.push(3); linkedList2.push(4);
    linkedList1.push(3)
    linkedList1.push(2)
    linkedList1.push(1)
    linkedList1.push(3); linkedList1.push(4);
    let result = linkedList1.intersection(linkedList2);
    expect(result).to.be.eql(true);
  });
  it('intersection 4', () => {
    let linkedList1 = new LinkedList(), linkedList2 = new LinkedList();
    linkedList1.push(1); linkedList2.push(2); linkedList2.push(3);
    linkedList1.push(2);linkedList2.push(3); linkedList2.push(4);
    linkedList1.push(3)
    linkedList1.push(2)
    linkedList1.push(1)
    linkedList1.push(3); linkedList1.push(4);
    let result = linkedList1.intersection(linkedList2);
    expect(result).to.be.eql(false);
  });

  // it('intersection 4', () => {
  //   let linkedList1 = new LinkedList();
  //   linkedList1.push(1);
  //   linkedList1.push(2);
  //   linkedList1.push(3);
  //   linkedList1.push(2);
  //   linkedList1.push(1);
  //   linkedList1.push(3); linkedList1.push(4);
  //   let result = linkedList1.isCircular();
  //   expect(result).to.be.eql(false);
  // });
});