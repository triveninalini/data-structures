const expect = require('chai').expect,
  Graph = require('../dataStructures/graph');

// begin a test suite of one or more tests
describe('Graph', function () {
  let graph = new Graph();

  it('breadth first approach, recursively------> no element found', async () => {
    let data = graph.bfs();
    expect(data).to.be.eql([]);
    expect(graph.adjacencyList).to.be.eql({});
  })

  it('adding vertex ----> first element', async () => {
    expect(graph.adjacencyList).to.be.eql({});
    graph.addVertex('mysore');
    expect(graph.adjacencyList).to.be.eql({ mysore: [] })
  });

  it('breadth first approach, recursively------> one element found', async () => {
    let data = graph.bfs();
    expect(data).to.be.eql(['mysore']);
   // expect(graph.adjacencyList).to.be.eql({ mysore: [] })
  })



  it('add edge', () => {
    graph.addEdge('mysore', 'bangalore');
    expect(graph.adjacencyList).to.be.eql({ mysore: ['bangalore'], bangalore: ['mysore'] })
  });
  it('breadth first approach, recursively------> one or more element found', async () => {
    let data = graph.bfs();
    expect(data).to.be.eql(['mysore', 'bangalore']);
  })

  it('add edge', () => {
    graph.addEdge('mysore', 'mandya');
    expect(graph.adjacencyList).to.be.eql({ mysore: ['bangalore', 'mandya'], bangalore: ['mysore'], mandya: ['mysore'] })
  });

  it('add edge', () => {
    graph.addEdge('bangalore', 'mandya');
    expect(graph.adjacencyList).to.be.eql({ mysore: ['bangalore', 'mandya'], bangalore: ['mysore', 'mandya'], mandya: ['mysore', 'bangalore'] })
  });
  
  it('add edge', () => {
    graph.addEdge('hubli', 'mandya');
    expect(graph.adjacencyList).to.be.eql({ mysore: ['bangalore', 'mandya'], bangalore: ['mysore', 'mandya'], mandya: ['mysore', 'bangalore', 'hubli'], hubli: ['mandya'] })
  });
  it('breadth first approach, recursively------> one or more element found', async () => {
    let data = graph.bfs();
    expect(data).to.be.eql(['mysore', 'bangalore', 'mandya', 'hubli']);
  })

  it('remove vertex', () => {
    graph.removeEdge('mysore', 'mandya');
    expect(graph.adjacencyList).to.be.eql({ mysore: ['bangalore'], bangalore: ['mysore', 'mandya'], mandya: ['bangalore', 'hubli'], hubli: ['mandya'] })
  });

  it('remove vertex', () => {
    graph.removeVertex('mysore');
    expect(Object.prototype.hasOwnProperty.call(graph.adjacencyList, 'mysore')).to.be.false;
    expect(graph.adjacencyList).to.be.eql({ bangalore: ['mandya'], mandya: ['bangalore', 'hubli'], hubli: ['mandya'] })
  });


});