const MultipleStacks = require('../dataStructures/multipleStacksFromArray'),
  expect = require('chai').expect;;

describe('checking multiple stack instance', () => {
  it('invalid data', () => {
    try {
      new MultipleStacks();
    } catch (ex) {
      expect(ex.message).to.eql('invalid data');
    }
  });
  it('valid data', () => {
    let msa = new MultipleStacks(3, 10);
    for (let i = 0; i < 3; i++) {
      expect(msa.values).to.eql([[], [], []]);
    }
  });
  it('push the data to stack', () => {
    let msa = new MultipleStacks(3, 10);
    msa.push(10, 1);
    expect(msa.values).to.be.eql([[], [10], []]);
    msa.push(10, 0);
    expect(msa.values).to.be.eql([[10], [10], []]);
    msa.push(10, 2);
    expect(msa.values).to.be.eql([[10], [10], [10]]);
  });
  it('negative test cases', () => {
    let msa = new MultipleStacks(3, 1);
    try {
      msa.push(10, 1);
    } catch (ex) {
      expect(ex.message).to.be.eql('stack does not exist')
    }
    msa.push(10, 0);
    try {
      msa.push(10, 0)
    } catch (ex) {
      expect(ex.message).to.be.eql('Stack overflow')
    }
  });
  it('pop the data', () => {
    let msa = new MultipleStacks(3, 10);
    let result = msa.pop(0);
    expect(result).to.be.eql(undefined);
    expect(msa.values).to.be.eql([[], [], []]);
    msa.push(10, 0);
    result = msa.pop(0);
    expect(result).to.be.eql(10);
    expect(msa.values).to.be.eql([[], [], []]);
    msa.push(10, 0);
    msa.push(20, 0);
    result = msa.pop(0);
    expect(result).to.be.eql(20);
    expect(msa.values).to.be.eql([[10], [], []]);
  });

  it('negative cases', () => {
    let msa = new MultipleStacks(1, 1);
    try {
      msa.pop(1);
    } catch(ex) {
      expect(ex.message).to.be.eql('invalid stack')
    }
  });
})