function binarySearch(arr, value){
  // add whatever parameters you deem necessary - good luck!
  let left = 0, right = arr.length -1, middle = Math.floor((right+left)/2);
  while(left <= right) {
      if(arr[middle] === value) return middle;
      if(arr[middle] > value) right = middle -1;
      else left = middle +1;
      middle = Math.floor((right+left)/2);
  } 
  return -1
}
// console.log(binarySearch([1,2,3,4,5],2))
// console.log(binarySearch([1,2,3,4,5],3))
// console.log(binarySearch([1,2,3,4,5],5))
// console.log(binarySearch([1,2,3,4,5],6))
let a = [5,6,10,13,14,18,30,34,35,37,40,44,64,79,84,86,95,96,98,99];
// console.log(binarySearch(a, 10))
// console.log(binarySearch(a, 100))
console.log(binarySearch(a, 95))
