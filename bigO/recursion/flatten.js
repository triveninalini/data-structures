
/***********************understand the problem ********************************************/

//restate the problem
//input : [1, 2, 3, [1, 2, 3, [1, 2]]] output: [1,2,3,1,2,3,1,2]
//what are the inputs that go into the problem?
//array
//what should be output?
//array
//how to determine outputs from inputs
//how should I label my inputs?
//input array, output flattened array

function flatten_practice(arr) {
	let result = [];
	for(let i = 0; i< arr.length; i++){
		if(Array.isArray(arr[i])){
			result = [...result, ...flatten(arr[i])];
		} else {
			result.push(arr[i]);
		}
	}
		return result;
	
}



/***********************concrete examples********************************************/

//start with simple examples, (utc with simple inputs)
console.log(flatten_practice([[1, 2, 3]])); //[1,2,3,4,5]
console.log(flatten_practice([1, 2, 3])); //[1,2,3]
console.log(flatten_practice([[1], [2], [3]]));
//go with the concreate examples
console.log(flatten_practice([1, 2, 3, 4, 5, [5, 6, [7, 8]]]));//[1,2,3,4,5, 5, 6, 7, 8]
console.log(flatten_practice([1, [2, 3, [6,7]],[[[[[[5]]]]]]]));//[1,5]
//explore with empty inputs
//explore with invalid inputs


/*************************Break it Down *******************************************/

//write down the actions that needs to be done
function flatten(oldArr){
  var newArr = []
  	for(var i = 0; i < oldArr.length; i++){
    	if(Array.isArray(oldArr[i])){
      		newArr = newArr.concat(flatten(oldArr[i]))
    	} else {
      		newArr.push(oldArr[i])
    	}
  } 
  return newArr;
}

/********************solve and simplify *******************************************/
/******************** Refactoring Questions *******************************************/

//can u check the result
//can u derive the result in different way
// can u understand it in a glance
// can use the result or method for some other problem
// can u improve the performance
// can u think other ways to refactor
// how other people have solved the problem