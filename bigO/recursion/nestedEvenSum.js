function nestedEvenSum(obj) {
  let arr = [];
  for(let key in obj) {
    if(obj.hasOwnProperty(key)) {
      let val = obj[key];
      if(typeof val === 'string') {
        arr.push(val)
      } else if(typeof val === 'object') 
        arr = [...arr, ...nestedEvenSum(val)]
    }
  }
  return arr;
}
var obj1 = {
  num: 1,
  test: [],
  data: {
      val: 4,
      info: {
          isRight: true,
          random: 66
      }
  }
}
const obj = {
  stuff: "foo",
  data: {
      val: {
          thing: {
              info: "bar",
              moreInfo: {
                  evenMoreInfo: {
                      weMadeIt: "baz"
                  }
              }
          }
      }
  }
}

var obj2 = {
  a: 2,
  b: {b: 2, bb: {b: 3, bb: {b: 2}}},
  c: {c: {c: 2}, cc: 'ball', ccc: 5},
  d: 1,
  e: {e: {e: 2}, ee: 'car'}
};
console.log(nestedEvenSum(obj));