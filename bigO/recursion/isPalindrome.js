function isPalindrome(str) {

  function reverse(str, index = -1) {
  
    if(index<0)  index = str.length -1;
    let reverseStr = str[index];
    if(index === 0) return reverseStr;
    return reverseStr + reverse(str, --index);
  }
  let reverseStr = reverse(str);
  return reverseStr === str;
}

console.log(isPalindrome('amanaplanacanalpanama'))