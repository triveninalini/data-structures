function recursive(arr, cb, index = 0) {
  if(index === arr.length-1) return cb(arr[index]);
  if(cb(arr[index])) return true;
  else return recursive(arr, cb, ++index);
}

const isOdd = val => val % 2 !== 0;
console.log(recursive([4,6,8,9], isOdd)) // true
