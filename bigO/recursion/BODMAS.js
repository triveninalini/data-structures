function factorial(num) {
  if(!num || num === 0) return 1;
  if(num === 1) return num;
  return num * factorial(num-1);
}

//power
function power(...args) {
  if(args[1] === 0) return 1;
    if(args[1] === 1) return args[0];
    return args[0] * power(args[0], --args[1]);
}

//product of array
function productOfArray(arr, index = 0) {
  if(arr.indexOf(0) >= 0) return 0;
  if(index === arr.length -1) return arr[index];
  return arr[index] * productOfArray(arr, ++index);
}

//write a function which accepts a number(num) and adds up all numbers from 0 to num.
function recursiveRange() {
  if(arguments[0] === 1) return 1;
  return arguments[0] + recursiveRange(--arguments[0])
}
//fibronicci series
//4
// function fib(num, n1 = 1, n2 =1) {
//   if(num === 0) return n2;
//   if(n1 ===1 && n2 ===1) num -= 2;
//   return fib(--num, n2, n2+n1);
// }
//better solution fo
function fib_better(n){
  if (n <= 2) return 1;
  return fib_better(n-1) + fib_better(n-2);
}
console.log(fib_better(4))
console.log(fib_better(10))
console.log(fib_better(28))
console.log(fib_better(35))