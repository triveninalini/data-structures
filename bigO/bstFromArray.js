const BinarySearchTree = require('../dataStructures/binarySearchTree');
//12345
//2, 1, 3
//12345 6 79801
function bstFromArray(arr) {
  let mid = Math.floor((arr.length - 1) / 2);
  let bst = new BinarySearchTree()
  bst.insert(arr[mid]);
  bst.insert(arr[1]);
  if (arr.length <= 1) return bst;
  bst.insert(arr[arr.length - 2]);
  for (let index = 0; index < arr.length; index++) {
    if (index !== 1 && index !== arr.length - 2) bst.insert(arr[index]);
  }
  console.log(bst);
}

function bstSortedArray(arr, first = 0, last = (arr.length - 1)) {
  let mid = Math.floor((first + last) / 2);
  
  let bst = new BinarySearchTree();
  bst.insert(arr[mid]);
  
  if (last > 0 && first < last) {
    bst.left = bstSortedArray(arr, first, mid -1);
    bst.right = bstSortedArray(arr, mid+1, last);
  }
  




let bst = bstSortedArray([1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12]);
console.log(bst);
//[1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15]
// 5, 2, 1, 0
//6,3,
