/***********************understand the problem ********************************************/

//restate the problem
//count the number of unique values in an sorted array
//what are the inputs that go into the problem?
//arr--> sorted array
//what should be output?
//count of unique values

//how to determine outputs from inputs
//how should I label my inputs?



/***********************concrete examples********************************************/

//start with simple examples, (utc with simple inputs)
//go with the concreate examples
//explore with empty inputs
//explore with invalid inputs


/*************************Break it Down *******************************************/

//write down the actions that needs to be done

function countUniqueValues(arr) {
  let count = 0
  for(let j = 0; j < arr.length; j++) {
    if(arr[j] !== arr[count]) {
      arr[++count] = arr[j];
    }
  }
  console.log(arr);
  return ++count;
}
console.log(countUniqueValues([1,2,3,4,4,4,4,54]));
/********************solve and simplify *******************************************/
/******************** Refactoring Questions *******************************************/

//can u check the result
//can u derive the result in different way
// can u understand it in a glance
// can use the result or method for some other problem
// can u improve the performance
// can u think other ways to refactor
// how other people have solved the problem