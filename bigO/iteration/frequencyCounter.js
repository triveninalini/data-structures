/***********************understand the problem ********************************************/

//restate the problem
//given array1, check whether array2 contains square of array1 and also it should have same frequency of occurence
//what are the inputs that go into the problem?

//what should be output?

//how to determine outputs from inputs
//how should I label my inputs?

function same(arr1, arr2) {
  if(!arr2 || !arr1 || !(arr2 || '').length || !(arr1 || '').length || (arr2 || []).length !== (arr1 || []).length) return false;
  let counter2  = {};

  for(const val of (arr2 || '')) {
    counter2[val] = (counter2[val] || 0) + 1;
  }

  for(const val of (arr1 || '')) {
    if(!counter2[val ** 2]) {
      return false
    }
    counter2[val] -= 1; 
  }
  return true;
}

/***********************concrete examples********************************************/

//start with simple examples, (utc with simple inputs)
console.log(same([2, 3], [4, 9]));//true
console.log((same([-2, 2, 5], [4, 4, 25])));//true
console.log((same([-2, 2, 5], [])));//false
console.log((same([-2, 5], [4, 4, 25])));//false
//go with the concreate examples
//explore with empty inputs
console.log(same([], []));//false
console.log(same());//false
//explore with invalid inputs


/*************************Break it Down *******************************************/

//write down the actions that needs to be done



/********************solve and simplify *******************************************/
/******************** Refactoring Questions *******************************************/

//can u check the result
//can u derive the result in different way
// can u understand it in a glance
// can use the result or method for some other problem
// can u improve the performance
// can u think other ways to refactor
// how other people have solved the problem