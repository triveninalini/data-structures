

/***********************understand the problem ********************************************/

//restate the problem
    //search a given element n in an array
//what are the inputs that go into the problem?
    //array and int
//what should be output?
   //result ---> index of n
//how to determine outputs from inputs
//how should I label my inputs?
[20, 30, 2, 40, 50, 1, 10,70, 0]

function binarySearch(arr, search) {
  //intialize first, last and middle index
  try{
  arr.sort();
  
  
  let first = 0, last = arr.length -1, middle = Math.floor(arr.length/2);
  while(first < last) {
    if(arr[middle] === search) return middle;
    if(arr[middle] < search) {
      first = middle;
      
    } else {
      last = middle;
    }
    middle = Math.floor((first+ last)/2);
  }

  }catch(ex){
    
    return null;
  }
}

/***********************concrete examples********************************************/

//start with simple examples, (utc with simple inputs)
console.log(binarySearch([20, 30, 2, 40, 50, 70, 0], 0));
console.log(binarySearch([20, 30, 2, 40, 50, 1, 10,70, 0], 30));
console.log(binarySearch());

//go with the concreate examples
//explore with empty inputs
//explore with invalid inputs
console.log(binarySearch([1], 3))

/*************************Break it Down *******************************************/

//write down the actions that needs to be done


/********************solve and simplify *******************************************/
/******************** Refactoring Questions *******************************************/

//can u check the result
//can u derive the result in different way
// can u understand it in a glance
// can use the result or method for some other problem
// can u improve the performance
// can u think other ways to refactor
// how other people have solved the problem