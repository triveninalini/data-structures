//-1, -2, 0,  2, 

function sumZero(arr) {
  try {
    arr || [].sort();
    //point to intial index as i
    //point to last index as l
    let left = 0, right = (arr || []).length -1;
    //loop through an array
    while (left < right) {
    //find the sum of arr[i]+arr[l], as sum  
    let sum = arr[left]+arr[right];
    //if sum is zero, return i, l
    if(sum === 0) return [ arr[left], arr[right]]
    //if sum is less than 0, increment i
    else if(sum < 0) left++;
    //if sum is greater than 0, decrement l
    else right--;
    }
    return [];
  } catch (e) {
    return e;
  }
}
/***********************understand the problem ********************************************/

//restate the problem
//what are the inputs that go into the problem?

//what should be output?

//how to determine outputs from inputs
//how should I label my inputs?

//finds the first pair of number, which is equal to zero
//input--->sorted array, output---> array of two

/***********************concrete examples********************************************/

//start with simple examples, (utc with simple inputs)
//console.log(sumZero([-1, -2, -3, 3, 2, 1]));
console.log(sumZero([-2, -1, 0,  1]));
console.log(sumZero([-2, -1, 0,  1, 2]));
console.log(sumZero([0, 1, 2, 3]));
console.log(sumZero([]));
console.log(sumZero());
//go with the concreate examples
//explore with empty inputs
//explore with invalid inputs


/*************************Break it Down *******************************************/

//write down the actions that needs to be done

/********************solve and simplify *******************************************/
/******************** Refactoring Questions *******************************************/

//can u check the result
//can u derive the result in different way
// can u understand it in a glance
// can use the result or method for some other problem
// can u improve the performance
// can u think other ways to refactor
// how other people have solved the problem