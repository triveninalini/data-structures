//given  two strings, check whether first string is subset second

function isSubSequence(s1, s2) {
  let index1 = 0;
  for(let index2 =0; index2< s2.length; index2++) {
    if(s1[index1] === s2[index2]) index1++;
    if(index1 === s1.length) return true;
  }
  return false;
}
console.log(isSubSequence('abc', 'acb'));
console.log(isSubSequence('abb', 'accbc'));
//https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/learn/quiz/4410590#questions