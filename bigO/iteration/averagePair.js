//given an sorted array and a target value(tv). return true if the sum of any two numbers is equal to target value

function averagePair(arr, tv) {
  if(!(arr || []).length && arr.length === 1) return false;
  let first = 0; last = arr.length -1;
  while(first < last) {
    if(arr[first] + arr[last] === tv) return true;
    if(arr[first] + arr[last] > tv) last--;
    else first++
  }
  return false;
}
console.log(averagePair([1,2,3], 2.5));

