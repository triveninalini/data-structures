

/***********************understand the problem ********************************************/

//restate the problem
    //find the max sum of num consecutive elements
//what are the inputs that go into the problem?
    //array, n
//what should be output?
   //result ---> consecutive elements
//how to determine outputs from inputs
//how should I label my inputs?

function maxSubArraySum(arr, num) {
  let maxSum = 0; 
  try {
  if(arr.length < num) return null;
  for(let index =0; index< num; index++) {
    maxSum += arr[index]
  }
  let tempSum = maxSum;
  for(let index = num; index < arr.length; index++) {
    tempSum = tempSum - arr[index -num] + arr[index];
    maxSum = Math.max(tempSum, maxSum);
  }
  return maxSum;
}catch(e) {
  console.error(e);
  return null;
}
}

/***********************concrete examples********************************************/

//start with simple examples, (utc with simple inputs)
console.log(maxSubArraySum([20, 30, 2, 40, 50, 70, 0], 2));
console.log(maxSubArraySum([20, 30, 2, 40, 50, 1, 10,70, 0], 3));
console.log(maxSubArraySum());

//go with the concreate examples
//explore with empty inputs
//explore with invalid inputs
console.log(maxSubArraySum([1], 3))

/*************************Break it Down *******************************************/

//write down the actions that needs to be done


/********************solve and simplify *******************************************/
/******************** Refactoring Questions *******************************************/

//can u check the result
//can u derive the result in different way
// can u understand it in a glance
// can use the result or method for some other problem
// can u improve the performance
// can u think other ways to refactor
// how other people have solved the problem