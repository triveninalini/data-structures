//if arrays are sorted
function areThereDuplicates(...args) {
  // good luck. (supply any arguments you deem necessary.)
  let counter = 0;
  for(let index = 1; index < args.length; index++) {
     if(args[counter] === args[index]) return true;
     else args[++counter] = args[index];
  }
  return false;
}

function isDuplicated() {
  let args = [...arguments];
  let frequencyCounter = {};
  for(let index = 0; index < args.length; index++) {
    if(frequencyCounter[args[index]]) return true;
    else frequencyCounter[args[index]] = 1;
  }
  return false
}
console.log(isDuplicated(1, 2, 3));
console.log(isDuplicated(1, 2, 2));
console.log(areThereDuplicates('a','a', 'b', 'c'));
//[1,0, -1, -2,3], i = 0; count = 0//[1,0,-1,-2, 3]
//while(stack)
//let x = stack.pop()
//x.val > arr[i+count] result[i] = arr[x.val], stack.pop(); stack.push(x);
//count++, stack.push(i+count);//1,3