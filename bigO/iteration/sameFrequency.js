function sameFrequency(num1,num2) {
  
  function getCounter(num1) {
    let counter = {};
    while(num1) {
      //123
      let digit1 = num1 % 10;
      counter[digit1] = counter[digit1] ?  counter[digit1] +1: 1;
      num1 = Math.floor(num1/10);
    }
    return counter;
  }
  try{
  if(num1 <= 0 || num1 <= 0) return null;
  let counter1 = getCounter(num1);
  let counter2 = getCounter(num2);
  for(let key in counter1) {
    if(counter2[key] !== counter1[key]) return false;
  }
  return true

}catch(ex) {
  console.log(ex);
}
}

console.log(sameFrequency(22, 222))
console.log(sameFrequency(22, 22))
console.log(sameFrequency(22, 23))
