function pivot(arr, first = 0, last = arr.length - 1) {
  let pivotIndex = first;
  function swap(i) {
    let temp = arr[pivotIndex];
    arr[pivotIndex] = arr[i];
    arr[i] = temp;
  }
  for (let i = first; i <= last; i++) {
    if (arr[first] > arr[i]) {
      ++pivotIndex;
      swap(i);
    }
  }

  swap(first);
  return pivotIndex;
}
function quickSort(arr, first = 0, last = arr.length - 1) {
  if (first <last) {
  last = pivot(arr, first, last);
  quickSort(arr, first, last-1)
  quickSort(arr, last + 1, arr.length -1);
  }
  return arr;
}
console.log(quickSort([10, 100, 9, -1, 22, 9999, 2002,20]));
//[1, 2, 20, 3, 30]
//when pivot index is equal arr.length -1, return the array
//


//[10, 100, 9, 20];
//[9, 10, 20, 100]