function mergeSort(arr) {
  let sorted = [];
  function merge(arr1, arr2) {
    
    let j = 0, i = 0; sorted = [], totallength = arr1.length + arr2.length;
    while (totallength !== sorted.length) {

      if (arr1[i] == null || (arr1[i] && arr2[j] < arr1[i])) {
        sorted.push(arr2[j]);
        j++;
      } else {
        sorted.push(arr1[i]);
        i++;
      }
    }
    return sorted;
  }
  let mid = Math.ceil(0+arr.length/2);
  if(arr.length <=1) return arr
  return merge(mergeSort(arr.splice(0,mid)), mergeSort(arr));
}
console.log(mergeSort([3,20, 1, 1000, 100, 0, 99]))
console.log(mergeSort([19, 100, 1, 200]));