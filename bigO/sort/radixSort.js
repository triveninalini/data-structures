function getMaxDigit(arr) {
  let max = -Infinity;
  for (let i = 0; i < arr.length; i++) {
    max = Math.max(arr[i], max)
  }
  return Math.floor(Math.log10(Math.abs(max))) + 1
}

//my solution
function radixSort(arr) {
  function getBucket(arr, digit = 0) {
    let buckets = [];
    //create the bucket
    //loop through the array
    //convert number to string, extract the number 
    for (let i = 0; i < arr.length; i++) {
      let n = arr[i].toString();
      let j = n.length - digit - 1;
      j = j < 0 ? 0 : n[j];
      buckets[j] = buckets[j] || [];
      buckets[j].push(arr[i]);
    }
    return buckets;
  }

  function sort(arr, i) {
    let buckets = getBucket(arr, i);
    return [].concat(...buckets)
  }
  let maxDigit = getMaxDigit(arr);
  for (let i = 0; i < maxDigit; i++) arr = sort(arr, i);
  return arr;
}

function rs(arr) {
  function getDigit(num, i) {
    return Math.floor(Math.abs(num) / Math.pow(10, i)) % 10;
  }

  let maxDigit = getMaxDigit(arr);
  for (let i = 0; i < maxDigit; i++) {
    let buckets = Array.from({length: maxDigit}, () => []);
    for (let j = 0; j < arr.length; j++) {
      let digit = getDigit(arr[j], i);
      buckets[digit] = buckets[digit] || [];
      buckets[digit].push(arr[j])
    }
    nums = [].concat(...buckets);
  }
  console.log(nums);
  nums = nums.filter(item => item != null)
  return nums;
}

console.log(rs([ 20, 0,22, 80, 24,99, 99, 25,123, 234, 23, 1]));