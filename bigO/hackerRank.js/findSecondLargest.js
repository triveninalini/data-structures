function getSecondLargest(nums) {
  // Complete the function
  if(!nums.length || (nums || []).length === 1) return null;
  let { largest, secondLargest } = nums[0]>nums[1] ? { largest: nums[0], secondLargest: nums[1]} :{ largest: nums[1], secondLargest: nums[0]}
  console.log(largest, secondLargest);
  for(let index = 3; index <nums.length; index++) {
      if(largest < nums[index]) {
          secondLargest = largest;
          largest = nums[index];
      } else  if(secondLargest < nums[index]) secondLargest = nums[index];
  }
  return secondLargest;
}

getSecondLargest([2,3,6,6,5]);