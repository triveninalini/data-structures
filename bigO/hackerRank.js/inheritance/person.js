function Person(name, age, gender = 'female') {
  this.name = name;
  this.age = age;
  this.gender = gender;
}

Person.prototype.greeting = function() {
  if(this.gender === 'male') console.log('Hi sir');
  else console.log('Hi mam');
}
function Teacher(name, age, gender, subject, DOB) {
  Person.call(this, name, age, gender);
  this.subject = subject;
}



  



Teacher.prototype = Object.create(Person.prototype);


Object.defineProperty(Teacher.prototype, 'constructor', { 
  value: Teacher, 
  enumerable: false, // so that it does not appear in 'for in' loop
  writable: true });
Teacher.prototype.displayName = function() {
    console.log(`Hi, I am ${this.name}`);
}
y = new Teacher('swapna', 50, 'female', 'social', '14-05-1965');



function Student(name, age, gender,  teacher) {
  Person.call(this, name, age, gender);
  this.teacher = teacher
} 
Student.prototype = Object.create(Person.prototype);
Object.defineProperty(Student.prototype, 'constructor', {
  value: Student,
  enumerable: false,
  writable: true
});
Student.prototype.displayName = function() {
  console.log(`Hi, I am ${this.name} and my teacher is ${this.teacher.name}`)
}
let s = new Student('triveni', 24, 'female', y)
console.log(s);s.displayName()
console.log(Object.keys(s));