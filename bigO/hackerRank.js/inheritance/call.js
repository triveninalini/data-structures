//Prototype.call
//example 1: in this example, { name: triveni} is binded to this of function display
function display() {
  console.log(this.name)
}
display({ name: triveni});//displays triveni

//In inheritance, this of child class will be sent to parent. so all the parent element gets binded to child