let LinkedList = require('../dataStructures/linkedList');

class CharacterStream {
  constructor() {
    this.characters = {};
    this.list = new LinkedList();
  }

  insert(char) {
    if (this.characters[char]) {
      if (this.characters[char] === 1) {
        if (this.list.head.data === char) {
          if (this.list.head.next) {
            this.list.head = this.list.head.next;
          } else {
            this.list.head = null;
            this.list.tail = null;
          }
          this.list.length--;
          return;
        };
        let node = this.list.head;

        let prev = node;
        while (node.data !== char) {
          prev = node;
          node = node.next;
        }
        prev.next = node.next;
        this.list.length--;
        this.characters[char] = this.characters[char]++;
      }
    } else {
      this.list.push(char);
      this.characters[char] = 1;
    }
  }

  findNextDuplicate() {
    return ((this.list || {}).head || {}).data;
  }
}

let charSet = new CharacterStream();
charSet.insert('h');
console.log('1');
charSet.insert('h');
console.log('1');
charSet.insert('l');
charSet.insert('o');
charSet.insert('l');
console.log('1');
console.log(charSet.findNextDuplicate());