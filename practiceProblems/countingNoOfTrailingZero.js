function countNoOfTrailingZero(n) {
  let count  = 0, factor = 5;
  while(n/factor) {
    count += Math.floor(n/factor);
    factor *= factor;
  }
  return count;
}
console.log(countNoOfTrailingZero(100));

//1 1 1 1 1 
//1 1 1 1 1 
//1 1 1 1 1 
//1 1 1 1 1 
//1 1 1 1 1 