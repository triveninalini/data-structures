async function sumZero(arr) {
  let result = [];
  function isValid(arr) {
    let is_all_negative = true, is_all_positive = true;
    is_all_negative = arr.every(item => item < 0);
    is_all_positive = arr.every(item => item > 0);
    return !(is_all_negative || is_all_positive)
  }
  async function arrToObj(arr) {
    let obj = {}, neg_values = [];
    for (let index = 0; index < arr.length; index++) {
      if (arr[index] < 0) neg_values.push(index);
      if (obj[arr[index]]) ++obj[arr[index]];
      else obj[arr[index]] = 0;
    }
    return { obj, neg_values };
  }
  let is_neg = await isValid(arr);
  if (!is_neg) return [];
  const { neg_values, obj } = await arrToObj(arr);
  console.log(`cmdk ${neg_values}`)
  for (let index = 0; index < neg_values.length; index++) {
    for (let arrIndex = 0; arrIndex < arr.length; arrIndex++) {
      if (neg_values[arrIndex] !== index) {
        let doublePairSum = ((arr[neg_values[index]] + arr[arrIndex] || 0) * -1), triplet = [];
        if (obj[doublePairSum]) {
          triplet = [arr[arrIndex], arr[neg_values[index]], doublePairSum];
        }
        console.log('###', triplet);
        if (triplet.length) result.push(triplet);
      }
    }
  }
  return result;
}
async function x() {
  console.log(await sumZero([1, 0, -2, 2]));
}
x();
